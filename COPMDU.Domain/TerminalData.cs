﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class TerminalData
    {
        public long Contract { get; set; }

        public string Terminal { get; set; }

        public string SerialAddress { get; set; }

        public string Address { get; set; }

        public NetworkData CableData { get; set; }

        public DigitalData GponData { get; set; }

        public bool SameContract { get; set; }

        public bool SameContractMdu { get; set; }

        public bool Mdu { get; set; }
    }
}
