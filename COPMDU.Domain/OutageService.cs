﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class OutageService
    {
        public int OutageId { get; set; }
        public int ServiceId { get; set; }
        public Outage Outage { get; set; }
        public Service Service { get; set; }
    }
}
