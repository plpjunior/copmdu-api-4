﻿using System.Collections.Generic;

namespace COPMDU.Domain
{
    public class NetworkData
    {
        public string Cmts { get; set; }

        public string Status { get; set; }

        public float? TempoOnline { get; set; }

        public string RegMode { get; set; }

        public string Placa { get; set; }

        public float? CmtsRecPower { get; set; }

        public float? CmtsSnr { get; set; }

        public string IpCable { get; set; }

        public string Uptime { get; set; }

        public List<int> F { get; set; }

        public List<float> Rx { get; set; }

        public List<float> Snr { get; set; }

        public float? TxCable { get; set; }

        public float? SnrCable { get; set; }

        public float? RxCable { get; set; }

        public string UpCable { get; set; }

        public string DownCable { get; set; }

        public string Profile { get; set; }

        public string Modelo { get; set; }

        public List<string> Ips { get; set; }

        public List<string> SfIds { get; set; }

        public int? UpIdx { get; set; }
    }
}