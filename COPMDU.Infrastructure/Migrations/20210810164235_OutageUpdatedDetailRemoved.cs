﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class OutageUpdatedDetailRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UpdatedDetail",
                table: "Outage");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDetail",
                table: "Outage",
                nullable: false);

        }
    }
}
