﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.InterationException
{

    [Serializable]
    public class NotMappedPageResponseException : System.Exception
    {
        public NotMappedPageResponseException(string page) : base($"Uma requisição HTTP retornou uma página não esperada \n{page}") { }
    }
}
