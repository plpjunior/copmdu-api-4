﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace COPMDU.Infrastructure.ClassAttribute
{
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class PageResultListPropertyAttribute : PageResultPropertyAttribute
    {
        public PageResultListPropertyAttribute(string regexString)
            : base(regexString)
        {
        }
    }
}
