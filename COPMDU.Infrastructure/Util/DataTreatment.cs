﻿using COPMDU.Domain;
using COPMDU.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace COPMDU.Infrastructure.Util
{

    /// <summary>
    /// Quebrar em tratamentos individuais
    /// </summary>
    public class DataTreatment
    {
        private readonly CopMduDbContext _db;
        private readonly List<Username> _usernames;
        private readonly List<OutageSymptom> _outageSymptoms;
        private readonly List<User> _users;
        private readonly List<OutageType> _outageTypes;
        private readonly List<City> _cities;
        private readonly List<SignalStatus> _signalStatus;
        private readonly List<SignalTerminal> _signalTerminals;
        private readonly List<Service> _services;
        private readonly IConfiguration _config;
        private readonly DbHelper _dbHelper;


        //TODO: quebrar o tratamento em classes individuais
        public DataTreatment(CopMduDbContext db, IConfiguration config, DbHelper dbHelper)
        {
            _db = db;
            _config = config;
            // dados para outage
            _usernames = _db.Username.ToList();
            _outageSymptoms = _db.OutageSymptom.ToList();
            _users = _db.User.ToList();
            _outageTypes = _db.OutageType.ToList();
            _cities = _db.City.ToList();
            _services = _db.Service.ToList();
            _dbHelper = dbHelper;
            //dados para sinais
            _signalStatus = _db.SignalStatus.ToList();
            _signalTerminals = _db.SignalTerminal.ToList();
        }

        /// <summary>
        /// Retorna o pimeiro grupo do primeiro match do regex recebido e procurado na string
        /// Caso não encontre devolve string em branco
        /// </summary>
        /// <returns></returns>
        private string ReturnFirstRegexGroup(string value, string regex)
        {
            var found = Regex.Matches(value, regex, RegexOptions.IgnoreCase);
            if (found.Count > 0)
                return found[0].Groups[1].Value;
            return null;
        }

        private City FindCityDb(string cidade) =>
            _cities.FirstOrDefault(c => c.Name.Equals(cidade));

        private string ExtractAddressFromObs(string Obs) =>
            ReturnFirstRegexGroup(" " + Obs, @"\W([AR].{0,2} .+?)[\#\<\\\/\-]") ?? "";

        private long ExtractContractFromDescription(string obs)
        {
            try
            {

                return long.Parse(
                    ReturnFirstRegexGroup(obs, @"CONTRATO.+?\d+?\/\s*(\d*-?\d)")?.Replace("-", "") ??
                    ReturnFirstRegexGroup(obs, @"CONTRATO.+?\s*(\d*-?\d)")?.Replace("-", "") ??
                    ReturnFirstRegexGroup(obs, @"(?:CONTRATO: [0-9]{4,10})")?.Replace("-", "") ??
                    // ReturnFirstRegexGroup(obs, @"(\d{8,9}-?\d?)")?.Replace("-", "") ??
                    "0");

            }
            catch (Exception)
            {
                //Valores astronomicos na obs fazendo impossível detectar o contrato correto
                return 0;
            }
        }

        private int ExtractNotificationFromDescription(string obs) =>
            int.Parse(
                ReturnFirstRegexGroup(obs, @"notificação\:?\s*(\d{6,7}?)\s") ??
                ReturnFirstRegexGroup(obs, @"notifica.+?(\d{6,7})") ??
                ReturnFirstRegexGroup(obs, @"atlas.+?(\d{6,7})") ??
                ReturnFirstRegexGroup(obs, @"not.+?(\d{6,7})") ??
                "0"
            );

        private bool CaseInsensitveContains(string text, string value) =>
            text.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) >= 0;

        private bool ExtractFilteredFromDescription(string Obs) =>
            CaseInsensitveContains(Obs, "filtrado");

        private DateTime ConvertDate(string value)
        {
            DateTime.TryParseExact(value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out DateTime dateOut);
            return dateOut;
        }

        private List<OutageService> IdToServices(List<Servico> servicos)
        {
            var outageServices = new List<OutageService>();
            foreach (var servico in servicos)
            {
                var id = servico.id;
                var service = _services.FirstOrDefault(s => s.Id == id);
                if (service == null)
                {
                    service = new Service() { Id = id, Name = "Serviço não Mapeado" };
                    _db.Add(service);
                    _db.SaveChanges();
                    _services.Add(service);
                }
                outageServices.Add(new OutageService() { Service = service });
            }
            return outageServices;
        }

        /// <summary>
        /// Trata informações de ticket e de ordem de serviço e retorna um objeto Outage já convertido e validado.
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="so"></param>
        /// <returns></returns>
        public Outage Treat(TicketResponse ticket, ServiceOrder so) =>
            new Outage
            {
                City = FindCityDb(ticket.cidade),
                Address = so?.END_COMPLETO ?? ExtractAddressFromObs(ticket.obs),
                Cep = so?.NUM_CEP,
                AddressComplement = so?.ID_COMPL1 ?? "",
                AddressComplement2 = so?.COMPL1_DESCR ?? "",
                Id = int.Parse(ticket.ticket),
                Description = ticket.ticket_descricao,
                IdMdu = int.Parse(ticket.cod_imovel_mdu),
                Node = ticket.abrangencias[1].valor,
                Contract = so?.NUM_CONTRATO ?? ExtractContractFromDescription(ticket.ticket_descricao),
                Notification = ExtractNotificationFromDescription(ticket.ticket_descricao),
                Title = ticket.titulo,
                PredictionDate = JavascriptInteration.ConvertJavascriptDateToDate(ticket.data_prev),
                StartDate = JavascriptInteration.ConvertJavascriptDateToDate(ticket.data_ini),
                ServiceOrderUpdatedTime = ConvertDate(so?.DT_UPDATE),
                UpdatedTime = DateTime.Now,
                Active = true,
                ServiceOrderTypeId = so != null ? (int?)int.Parse(so.ID_TIPO_OS) : null,
                OpenedBy = _dbHelper.ReturnBase(ticket.usuario, _usernames, _db.Username),
                Symptom = _dbHelper.ReturnBase(ticket.sintoma, _outageSymptoms, _db.OutageSymptom),
                TechnicUser = so != null ? _dbHelper.ReturnUser(so.WO_EQUIPE_LOGIN, _users, _db.User) : null,
                Type = _dbHelper.ReturnBase(ticket.titulo, _outageTypes, _db.OutageType),
                AffectedServices = IdToServices(ticket.servicos),
                Filtered = ExtractFilteredFromDescription(ticket.ticket_descricao),
                InsertDate = DateTime.Now,
                TechnicianUpdatedTime = string.IsNullOrEmpty(so?.WO_EQUIPE_LOGIN) ? null : (DateTime?)DateTime.Now,
            };

        public List<Outage> TreatList(List<TicketResponse> tickets, List<ServiceOrder> sos)
        {
            var outages = new List<Outage>();
            foreach (var ticket in tickets)
            {
                var contract = ExtractContractFromDescription(ticket.ticket_descricao);
                var so = sos.FirstOrDefault(s => s.COD_IMOVEL.Equals(ticket.cod_imovel_mdu) && (contract == 0 || contract == s.NUM_CONTRATO));
                if (so == null && contract > 0)
                    so = sos.FirstOrDefault(s => s.NUM_CONTRATO.Equals(contract));

                outages.Add(Treat(ticket, so));
            }
            return outages;
        }

        public string GetDictionary(IDictionary<string, object> dictionary, string key) =>
            dictionary != null ?
                (dictionary.ContainsKey(key) ? dictionary[key].ToString() : null) :
                "";

        private float ExtractValue(IDictionary<string, object> dictionary, string key) =>
            float.TryParse(GetDictionary(dictionary, key), NumberStyles.Float, CultureInfo.InvariantCulture, out var result) ?
            result :
            0.0F;

        public static int GetDictionaryPosition(string key)
        {
            var extractDigits = Regex.Match(key, @"\d+").Value;
            return extractDigits == "" ?
                0 :
                int.Parse(extractDigits, CultureInfo.InvariantCulture);
        }


        private List<T> ExtractValues<T>(IDictionary<string, object> dictionary, string key) where T : SignalValue
        {
            if (dictionary == null)
                return null;
            var list = new List<T>();
            var values = new List<float>();
            var currentKey = key;
            while (dictionary.ContainsKey(currentKey))
            {
                values.Add(ExtractValue(dictionary, currentKey));
                var digit = GetDictionaryPosition(currentKey);
                currentKey = $"{key}_{digit + 1}";
            }

            foreach (var value in values)
            {
                var returnObj = (T)Activator.CreateInstance(typeof(T));
                returnObj.Value = value;
                list.Add(returnObj);
            }

            return list;
        }

        public OutageSignal Treat(ResultadoPoolingVizinhos.Item item) =>
            new OutageSignal
            {
                Contract = item.Contrato,
                Address = item.Endereco,
                Mac = item.MAC,
                Terminal = _dbHelper.ReturnBase(item.Terminal, _signalTerminals, _db.SignalTerminal),
                Status = _dbHelper.ReturnBase(GetDictionary(item.CableData, "STATUS"), _signalStatus, _db.SignalStatus),
                CmtsSnr = ExtractValue(item.CableData, "CMTS_SNR"),
                CmTx = ExtractValue(item.CableData, "TXCABLE"),
                CmSnr = ExtractValues<SignalCmSnr>(item.CableData, "SNR"),
                CmRx = ExtractValues<SignalCmRx>(item.CableData, "RX")
            };
        public List<OutageSignal> TreatList(List<ResultadoPoolingVizinhos.Item> itens)
        {

            var signals = new List<OutageSignal>();
            if (itens != null)
                foreach (var item in itens)
                    signals.Add(Treat(item));

            return signals;
        }

    }
}
