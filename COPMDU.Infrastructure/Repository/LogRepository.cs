﻿using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace COPMDU.Infrastructure.Repository
{
    public class LogRepository : ILogRepository
    {
        private readonly CopMduDbContext _db;

        public LogRepository(CopMduDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Log> GetAll()
        {
            return _db.Log;
        }

        public Log Get(int id)
        {
            return _db.Log.Find(id);
        }

        public void Add(Log entity)
        {
            entity.Date = DateTime.Now;
            _db.Add(entity);
            _db.SaveChanges();
        }

        public void Update(Log entity)
        {
            _db.Update(entity);
            _db.SaveChanges();
        }
        public void RecordException(Exception ex, int? ticket = null)
        {
            try
            {
                Add(new Log() { Message = ex.Message, StackTrace = ex.StackTrace, Type = 4, Ticket = ticket });
            }
            catch (Exception)
            {
                //Erros ao gravar exceção nao podem interromper a operação.
            }
        }

        public void Delete(Log entity)
        {
            _db.Remove(entity);
            _db.SaveChanges();
        }
    }
}
