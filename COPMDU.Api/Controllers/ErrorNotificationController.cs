﻿using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COPMDU.Api.Controllers
{
    /// <summary>
    /// Api de controle de erros
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ErrorNotificationController : ControllerBase
    {
        private readonly IErrorNotification _errorNotification;

        /// <summary>
        /// Api de controle de erros
        /// </summary>
        /// <param name="errorNotification"></param>
        public ErrorNotificationController(IErrorNotification errorNotification)
        {
            _errorNotification = errorNotification;
        }

        /// <summary>
        /// Adiciona mensagem de erro
        /// </summary>
        /// <param name="transport"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("AddMessage")]
        public async Task<bool> AddMessage([FromBody]NotificationErrorTransport transport) =>
            await _errorNotification.Insert(transport.Message, transport.UserId, transport.ReasonId, transport.Ticket);

        /// <summary>
        /// Retornas razoes possiveis de erro
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("GetReasons")]
        public List<ErrorReportReason> GetReasons() =>
            _errorNotification.GetReasons();

        /// <summary>
        /// Classe para transportar notificacoes de erro
        /// </summary>
        public class NotificationErrorTransport
        {
            /// <summary>
            /// Mensagem de erro
            /// </summary>
            public string Message { get; set; }

            public int ReasonId { get; set; }

            public int Ticket { get; set; }

            public int UserId { get; set; }
        }
    }
}
